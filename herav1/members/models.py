from django.db import models
from django.urls import reverse


H_MONTHS = (
        ('1','January'),
        ('2','February'),
        ('3','March'),
        ('4','April'),
        ('5','May'),
        ('6','June'),
        ('7','July'),
        ('8','August'),
        ('9','September'),
        ('10','October'),
        ('11','November'),
        ('12','December'),
    )

class Member(models.Model):
    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200)
    birthyear = models.IntegerField(null=True, blank=True)
    birthmonth = models.CharField(max_length=2, choices=H_MONTHS,null=True)
    birthday = models.IntegerField(null=True, blank=True)
    
    def __str__(self):
        return self.firstname


    def get_absolute_url(self):
        return reverse('member_edit', kwargs={'pk': self.pk})
