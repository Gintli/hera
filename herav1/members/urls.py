from django.contrib import admin
from django.conf import settings # new
from django.urls import path, include #add include
from django.conf.urls.static import static # new

from . import views

urlpatterns = [
    path('', views.MemberList.as_view(), name='member_list'),
    path('view/<int:pk>', views.MemberView.as_view(), name='member_view'),
    path('new', views.MemberCreate.as_view(), name='member_new'),
    path('view/<int:pk>', views.MemberView.as_view(), name='member_view'),
    path('edit/<int:pk>', views.MemberUpdate.as_view(), name='member_edit'),
    path('delete/<int:pk>', views.MemberDelete.as_view(), name='member_delete'),
    #path('', include(Member.urls))
]
