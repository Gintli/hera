from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

from members.models import Member


class MemberList(ListView):
    model = Member

class MemberView(DetailView):
    model = Member

class MemberCreate(CreateView):
    model = Member
    fields = ['firstname', 'lastname', 'birthyear','birthmonth','birthday']
    success_url = reverse_lazy('member_list')

class MemberUpdate(UpdateView):
    model = Member
    fields = ['firstname', 'lastname', 'birthyear','birthmonth','birthday']
    success_url = reverse_lazy('member_list')

class MemberDelete(DeleteView):
    model = Member
    success_url = reverse_lazy('member_list')
    

