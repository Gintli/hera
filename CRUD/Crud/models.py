from django.db import models

class Post(models.Model):
 title = models.CharField(max_length=120, help_text='title of message.')
 message = models.TextField(help_text="what's on your mind ...")
 firstname = models.CharField(max_length=255, help_text='First Name of Family Member.')
 lastname = models.CharField(max_length=255, help_text='Last Name of Family Member.')
 birthdate = models.DateTimeField()
 
class CoverPic(models.Model):
    title = models.TextField()
    cover = models.ImageField(upload_to='images/')

def __str__(self):
  return self.title